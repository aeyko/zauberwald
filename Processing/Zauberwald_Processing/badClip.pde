class badClip{
  
  boolean isRunning;
  
  float opacity;
  float volume;
  
  badClip(){
  
    this.isRunning = false;
    
  }
  
  void setup(){
    
    this.stop();
    this.setVolume(1.0);
    this.setOpacity(1.0);
  
  }
  
  void draw(){
  
  }
  
  void start(){
    
    if(!this.isRunning){
      OscMessage myMessage = new OscMessage("/layer2/clip1/connect");
      myMessage.add(1);
      oscP5.send(myMessage, myRemoteLocation);
      
      this.isRunning = true;
    }
    
  }
  
  void unpause(){
    OscMessage myMessage = new OscMessage("/activeclip/audio/position/direction");
    myMessage.add(1);
    oscP5.send(myMessage, myRemoteLocation);
  }
  
  void pause(){
    OscMessage myMessage = new OscMessage("/activeclip/audio/position/direction");
    myMessage.add(2);
    oscP5.send(myMessage, myRemoteLocation);
  }
  
  void stop(){
    
    OscMessage myMessage = new OscMessage("/layer2/clip2/connect");
    myMessage.add(1);
    oscP5.send(myMessage, myRemoteLocation);
    
    this.isRunning = false;
      
  }
  
  
  void setOpacity(float o){
  
    this.opacity = o;
    
    OscMessage myMessage = new OscMessage("/layer2/video/opacity/values");
    myMessage.add(this.opacity);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void mute(){
    
    this.volume = 0.0;
    
    OscMessage myMessage = new OscMessage("/layer2/audio/volume/values");
    myMessage.add(this.volume);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void unmute(){
    
    this.volume = 1.0;
    
    OscMessage myMessage = new OscMessage("/layer2/audio/volume/values");
    myMessage.add(this.volume);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void setVolume(float v){
  
    this.volume = v;
    
    OscMessage myMessage = new OscMessage("/layer2/audio/volume/values");
    myMessage.add(this.volume);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
}