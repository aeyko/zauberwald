import netP5.*;
import oscP5.*;
import processing.serial.*;

Serial serialPort;
 
OscP5 oscP5;
NetAddress myRemoteLocation;

String port;

int chairLastRead, lastVisualCheck, appStartTime;
boolean chairStatus;
boolean resetup = false;

float activeClipPos;

goodClip goodClip = new goodClip();
badClip badClip = new badClip();
transition transition = new transition();

void setup(){
  
  appStartTime = millis();
  
  size(200,200);
  fill(0);
  
  frameRate(25);
  oscP5 = new OscP5(this, 12001);
  myRemoteLocation = new NetAddress("127.0.0.1", 12000);
  
  //Serial
  
  //println(Serial.list());
  for (int i = 0; i < Serial.list().length; i++) {
    if(Serial.list()[i].indexOf("/dev/cu.usbmodem") == 0){
      port = Serial.list()[i];
    }
  }
  serialPort = new Serial(this, port, 9600);
  
  
  //Setup Resolume
  goodClip.setup();
  badClip.setup();
  transition.setup();
}

void draw() {
  
  background(0);
  fill(255);
  if(chairStatus)
    ellipse(50,50,50,50);
  
  // 5 Min in millis -> Resetup
  if(millis() - appStartTime >= 300000 && resetup == false){
    reSetup();
    resetup = true;
  }
  
  goodClip.draw();
  badClip.draw();
  transition.draw();
  
}

void reSetup(){
  goodClip.setup();
  badClip.setup();
  transition.setup();
}

void chairAction(){
  if(chairStatus == true){
    
    transition.start();
    //badClip.start();
    goodClip.mute();
    goodClip.setAction(1);
  
  } else{
    
    transition.stop();
    badClip.stop();
    
    goodClip.unmute();
    goodClip.setAction(2);
  
  }
}

void serialEvent(Serial myPort){
  if (myPort.available() > 0){
    String completeString = myPort.readStringUntil(10);
    if (completeString != null){
      String s = trim(completeString);
      int i = int(s);
      chairStatus = boolean(i);
      chairLastRead = millis();
      
      chairAction();
    }
  }
}

void oscEvent(OscMessage theOscMessage) {
  
  //aktuelle Pos Video
  if(theOscMessage.checkAddrPattern("/activeclip/video/position/values")){
    if(theOscMessage.checkTypetag("f")) {
      activeClipPos = theOscMessage.get(0).floatValue();
    }
  }
  
}


//Debug Mode
void keyPressed() {
  if (key == ' ') {
    if(chairStatus == true){
      println("debug end");
      chairStatus = false;
      chairAction();
    } else{
      println("debug start");
      chairStatus = true;
      chairAction();
    }
  }
}