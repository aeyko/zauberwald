class transition{

  boolean isRunning;
  int startTime;
  
  int runTime = 3050;
  
  int randomMin = 1;
  int randomMax = 8;
  
  float opacity;
  float volume;
  
  
  
  transition(){
    
    this.isRunning = false;
    this.opacity = 1.0;
    this.volume = 1.0;
    
  }
  
  
  void setup(){
  
    this.stop();
    this.setVolume(1.0);
    this.setOpacity(1.0);
    
  }
  
  void draw(){
    
    if(this.isRunning && millis() - startTime >= runTime){
      
      this.stop();
      badClip.start();
      
    }
  
  }
  
  void start(){
    
    if(!this.isRunning){
      //println(this.getRandomClip());
      OscMessage myMessage = new OscMessage(this.getRandomClip());
      myMessage.add(1);
      oscP5.send(myMessage, myRemoteLocation);
      
      this.isRunning = true;
      this.startTime = millis();
    }
    
  }
  
  void stop(){
    
    if(this.isRunning){
    
      OscMessage myMessage = new OscMessage("/layer1/clip9/connect");
      myMessage.add(1);
      oscP5.send(myMessage, myRemoteLocation);
      
      this.isRunning = false;
      
    }
  }
  
  void setOpacity(float o){
  
    this.opacity = o;
    
    OscMessage myMessage = new OscMessage("/layer1/video/opacity/values");
    myMessage.add(this.opacity);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void setVolume(float v){
  
    this.volume = v;
    
    OscMessage myMessage = new OscMessage("/layer1/audio/volume/values");
    myMessage.add(this.volume);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  String getRandomClip(){
    float r = random(this.randomMin, this.randomMax);
    String s = "/layer1/clip" + str(round(r)) + "/connect";
    return s;
  }
  
}