class goodClip{
  
  
  boolean isRunning;
  float lastPos;
  
  float opacity;
  float volume;
  
  int action; // 0: nothing | 1: fadeOut | 2: fadeIn

  goodClip(){
    
    this.isRunning = false;
    this.lastPos = 0.0;
    
    this.opacity = 1.0;
    this.volume = 1.0;
    
    this.action = 0;
    
  }
  
  void setup(){
    
    this.setVolume(1.0);
    this.setOpacity(1.0);
    
    this.stop();
    this.start();
  
  }
  
  void draw(){
  
    switch(action){
      
      case 1:
        this.fadeOut();
        break;
        
      case 2:
        this.fadeIn();
        break;
      
    }
  
  }
  
  void start(){
    if(!this.isRunning){
    
      OscMessage myMessage = new OscMessage("/layer3/clip1/connect");
      myMessage.add(1);
      oscP5.send(myMessage, myRemoteLocation);
      
      OscMessage myMessage2 = new OscMessage("/activeclip/video/position/values");
      myMessage2.add(this.lastPos);
      oscP5.send(myMessage2, myRemoteLocation);
      
      this.isRunning = true;
    
    }
  }
  
  void stop(){
      
    this.lastPos = activeClipPos;
  
    OscMessage myMessage = new OscMessage("/layer3/clip2/connect");
    myMessage.add(1);
    oscP5.send(myMessage, myRemoteLocation);
    
    this.isRunning = false;
    
  }
  
  void fadeOut(){
    if(this.opacity > 0 && action == 1){
      this.opacity = this.opacity - 0.1;
      
      OscMessage myMessage = new OscMessage("/layer3/video/opacity/values");
      myMessage.add(this.opacity);
      oscP5.send(myMessage, myRemoteLocation);
      
    } else if(this.opacity <= 0 && action == 1){
      this.action = 0;
    }
  }
  
  void fadeIn(){
    if(this.opacity < 1 && action == 2){
      this.opacity = this.opacity + 0.1;
      
      OscMessage myMessage = new OscMessage("/layer3/video/opacity/values");
      myMessage.add(this.opacity);
      oscP5.send(myMessage, myRemoteLocation);
      
    } else if(this.opacity >= 1 && action == 2){
      this.action = 0;
    }
  }
  
  void setOpacity(float o){
  
    this.opacity = o;
    
    OscMessage myMessage = new OscMessage("/layer3/video/opacity/values");
    myMessage.add(this.opacity);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void mute(){
    
    this.volume = 0.0;
    
    OscMessage myMessage = new OscMessage("/layer3/audio/volume/values");
    myMessage.add(this.volume);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void unmute(){
    
    this.volume = 1.0;
    
    OscMessage myMessage = new OscMessage("/layer3/audio/volume/values");
    myMessage.add(this.volume);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void setVolume(float v){
  
    this.volume = v;
    
    OscMessage myMessage = new OscMessage("/layer3/audio/volume/values");
    myMessage.add(this.volume);
    oscP5.send(myMessage, myRemoteLocation);
    
  }
  
  void setAction(int a){
    this.action = a;
  }

}