
import processing.serial.*;

Serial serialPort;

String chairStatus;

// Classes
ArrayList<Circle> circles = new ArrayList<Circle>();

final int INITIAL_NBRE_OF_CIRCLES = 1;

// Int
int nbreOfCircle = INITIAL_NBRE_OF_CIRCLES;
int ATTRACTING_INITIAL_CIRCLE_SIZE, 
    REJECTING_INITIAL_CIRCLE_SIZE; 

// floats
float lastTime = millis();

// Bool
boolean isAttracting = true; 

void setup(){
  //size(500,500);
  smooth(32);
  fullScreen();
  background(0);
  frameRate(200);
  
  ATTRACTING_INITIAL_CIRCLE_SIZE = int(width * 1.2); 
  REJECTING_INITIAL_CIRCLE_SIZE = 0; 
  
  //Serial
  //println(Serial.list());
  serialPort = new Serial(this, Serial.list()[3], 9600);
}

void draw() {
  if(int(chairStatus) == 0) {
    isAttracting = true;
  } else {
    isAttracting = false;
  }
  
  drawExperience();
}

void serialEvent(Serial myPort)
{
  if (myPort.available() > 0)
  {
    String completeString = myPort.readStringUntil(10);
    if (completeString != null){
      chairStatus = trim(completeString);
    }
  }
}

void createNewCircle() {
  circles.add(new Circle(isAttracting ? ATTRACTING_INITIAL_CIRCLE_SIZE : REJECTING_INITIAL_CIRCLE_SIZE));
}

void drawExperience () {
  background(0);
  
  // Create a new enemy every Xsec
  if (millis() - lastTime > 300) {
    lastTime = millis(); 
    createNewCircle();
  }
  
  for(int i = 0; i < circles.size(); i++) {
    circles.get(i).onUpdate();
    
    if(circles.get(i).isDead) {
      circles.remove(i);
    }
  }
}

void drawRejectingExperience () {
  
}