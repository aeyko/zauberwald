class Circle {
 
  PVector position = new PVector(width/2, height/2);  
  
  int steps = 100,
      incrRatio = 10; 
  
  int size,
      strokeWeight = 6,
      opacity = 255;
      
  int scaleRatio;
  
  boolean isDead = false; 
  
  Circle(int originalSize) {
    size = originalSize;
    scaleRatio = originalSize / steps;
  }
  
  void drawShape() {
   pushStyle();
     noFill();
     stroke(color(255, 255, 255, opacity));
     strokeWeight(strokeWeight);
     ellipse(position.x, position.y, size, size);  
   popStyle();
  }
  
  void setSize() {   
   if(isAttracting) {
     size -= incrRatio;
     
     if(size <= 0) {
       kill();
     }
   } else {
     size += incrRatio;
     
     if(size >= ATTRACTING_INITIAL_CIRCLE_SIZE) {
       kill();
     }
   }
   
   opacity -= 0.00001; 
   
   if(opacity <= 0) {
    opacity =0;  
   }
  }
  
  void kill() {
    isDead = true;
  }
  
  void onUpdate() {
   setSize();
   drawShape();
  }
}