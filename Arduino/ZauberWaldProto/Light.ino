#define TRANSITION_SPEED = 500;
const int PINS_RGB[3] = {3,5,6};

boolean GoingUp = true;
int R, G, B;

void LIGHT_setup(){
  //pinMode();
}

void LIGHT_loop() {
  // Set RGB Values New
  if(prevValue != isDark) {
    if(isDark == true){
      R = B;
      G = 0;
      B = 0;
    } else if(isDark == false){
      B = R;
      R = 0;
      G = 0;
    }
  }

  // Loops
  if(isDark == true){
    badLoop();
  } else if(isDark == false){
    goodLoop();
  }

  //analogWrite(PINS_RGB[0], R);
  //analogWrite(PINS_RGB[1], G);
  //analogWrite(PINS_RGB[2], B);
  
}

void goodLoop(){

  if(GoingUp == true){
    R = R + 1;
  } else{
    R = R - 1;
  }

  if(R > 255){
    R = 255;
  } else if(R < 0){
    R = 0;
  }

  if(R >= 255 && GoingUp == true){
    GoingUp = false;
  } else if(R <= 0 && GoingUp == false){
    GoingUp = true;
  }
  
}

void badLoop(){
  if(GoingUp == true){
    B = B + 10;
  } else{
    B = B - 10;
  }

  if(B > 255){
    B = 255;
  } else if(B < 0){
    B = 0;
  }

  if(B >= 255 && GoingUp == true){
    GoingUp = false;
  } else if(B <= 0 && GoingUp == false){
    GoingUp = true;
  }
}
