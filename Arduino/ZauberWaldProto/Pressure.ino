const int NBRE_OF_SENSORS = 3; 
const int PINS[3] = {A1, A2, A3};
int lastRead;

void PRS_loop() {

  //Check if Time past for Delay
  if(millis() - lastRead >= DELAY_BETWEEN_CHECKS){

    // read value from sensors
    int _val1 = PRS_getPressureValue(0),
        _val2 = PRS_getPressureValue(1),
        _val3 = PRS_getPressureValue(2);
  
    // Check if one one them is bigger than the min value. 
    if(_val1 >= MIN_VALUE_TO_TRIGGER || _val2 >= MIN_VALUE_TO_TRIGGER || _val3 >= MIN_VALUE_TO_TRIGGER) {
      isDark = true; 
    } else {
      isDark = false; 
    }
  
    // Send value to processing only when it changed from the previous value
    if(prevValue != isDark) {
      Serial.println(isDark);
      prevValue = isDark;
    }
  
    // set back the previous value
    prevValue = isDark;

    //set the last Read
    lastRead = millis();
  }
  
}

int PRS_getPressureValue(int _index) {
  return analogRead(PINS[_index]);
}

