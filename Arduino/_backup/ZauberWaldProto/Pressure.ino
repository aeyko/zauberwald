const int PRESSURE_PIN = A1;

void PRS_setup() {
  
}

void PRS_loop() {
  // Read value from pressure sensor
  int _val = PRS_getPressureValue();  
  if(_val >= MIN_VALUE_TO_TRIGGER) {
    isDark = true; 
  } else {
    isDark = false; 
  }

  if(prevValue != isDark) {
    //Serial.println(isDark);    
    prevValue = isDark;
  }
  Serial.println(_val);
  delay(DELAY_BETWEEN_CHECKS);

  prevValue = isDark;
}

int PRS_getPressureValue() {
  return analogRead(PRESSURE_PIN);
}

